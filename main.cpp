#include <iostream>

using namespace std;

bool check_if_fail(int choose_1);

bool exit_flag(int choose);

bool check_if_fail(int choose_1, int choose_2);

void swap(int &a, int &b);

int main() {
    int a = 1;
    int b = 2;
    int c = 3;
    int choose_1;
    int choose_2;

    cout << "***ZAMIENIANIE LICZB***" << endl;
    cout << "Wpisz \"9\" aby wyjsc" << endl;

    for (;;) {
        cout << "1.a = " << a << endl;
        cout << "2.b = " << b << endl;
        cout << "3.c = " << c << endl;
        cout << "Wybierz zmienne do zamiany" << endl;

        cin >> choose_1;

        while (check_if_fail(choose_1)) {
            if (exit_flag(choose_1)) {
                break;
            }
            cout << "Wybrano niepoprawna opcje, sprobuj jeszcze raz" << endl;
            cin >> choose_1;
        }

        if (exit_flag(choose_1)) {
            break;
        }

        cin >> choose_2;

        while (check_if_fail(choose_1, choose_2)) {
            if (exit_flag(choose_2)) {
                break;
            }
            cout << "Wybrano niepoprawna opcje, sprobuj jeszcze raz" << endl;
            cin >> choose_2;
        }

        if (exit_flag(choose_2)) {
            break;
        }

        int chooseOutcome = choose_1 + choose_2;

        switch (chooseOutcome) {
            case 3:
                swap(a, b);
                break;
            case 4:
                swap(a, c);
                break;
            case 5:
                swap(b, c);
                break;
        }
    }
    return 0;
}

bool check_if_fail(int choose_1) {
    if (cin.fail() || choose_1 < 1 || choose_1 > 3) {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        return true;
    }
    return false;
}

bool exit_flag(int choose) {
    if (choose == 9) {
        return true;
    }
    return false;
}

bool check_if_fail(int choose_1, int choose_2) {
    if (cin.fail() || choose_2 < 1 || choose_2 > 3 || choose_2 == choose_1) {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        return true;
    }
    return false;
}

void swap(int &a, int &b) {
    int c = a;
    a = b;
    b = c;
}